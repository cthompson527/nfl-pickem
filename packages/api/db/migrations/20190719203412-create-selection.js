'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'selections',
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()'),
        },
        user_id: {
          allowNull: false,
          type: Sequelize.STRING,
          references: {
            model: {
              tableName: 'users',
            },
          },
        },
        game_id: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: {
              tableName: 'games',
            },
          },
        },
        team_id: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: {
              tableName: 'teams',
            },
          },
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
      },
      {
        uniqueKeys: {
          unique_user_selection: {
            fields: ['user_id', 'game_id'],
          },
        },
      },
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('selections');
  },
};
