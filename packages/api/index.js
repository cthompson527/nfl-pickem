require('dotenv').config();
const jwt = require('express-jwt');
const hasPermission = require('./permission-middleware');
const jwksRsa = require('jwks-rsa');
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const postGames = require('./handlers/postGames');
const games = require('./handlers/games');
const opponentSelections = require('./handlers/opponentSelections');
const users = require('./handlers/users');
const selections = require('./handlers/selections');
const postSelections = require('./handlers/postSelections');
const compression = require('compression');

app.use(compression());
app.use(cors());
app.use(bodyParser.json());

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`,
  }),

  audience: process.env.AUTH0_AUDIENCE,
  issuer: `https://${process.env.AUTH0_DOMAIN}/`,
  algorithms: ['RS256'],
});

const canCreateGames = hasPermission('create:games');

app.get('/games', checkJwt, games);
app.get('/selections', checkJwt, selections);
app.get('/opponentSelections', checkJwt, opponentSelections);
app.get('/users', checkJwt, users);
app.post('/selections', checkJwt, postSelections);
app.post('/games', checkJwt, canCreateGames, postGames);

app.listen(8080);
