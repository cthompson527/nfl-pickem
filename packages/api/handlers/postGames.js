const db = require('../db/models/index');
const { team, game, week } = db;

const postGamesHandler = async (req, res) => {
  const updatedGames = req.body.games;
  console.log(req.body);
  await Promise.all(
    req.body.map(({ id, ...gameWithoutID }) =>
      game.update(gameWithoutID, { where: { id } }),
    ),
  );
  res.json({ success: true });
};

module.exports = postGamesHandler;
