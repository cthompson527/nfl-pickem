const db = require('../db/models/index');
const { user } = db;

const users = async (req, res) => {
  res.json(await user.findAll());
};

module.exports = users;
