const db = require('../db/models/index');
const { selection, team, game, user } = db;

const opponentSelectionsHandler = async (req, res) => {
  const userID = req.query.id;
  if (userID) {
    return res.json(
      await selection.findAll({
        where: { user_id: userID },
        include: [
          team,
          { model: game, where: { time_played: { $lt: new Date() } } },
        ],
      }),
    );
  }
  const selections = await selection.findAll({
    attributes: ['user_id', 'game_id', 'team_id'],
    include: [
      user,
      { model: game, where: { time_played: { $lt: new Date() } } },
    ],
  });
  return res.json(
    selections.map(s => ({
      user_id: s.user_id,
      game_id: s.game_id,
      team_id: s.team_id,
    })),
  );
};

module.exports = opponentSelectionsHandler;
