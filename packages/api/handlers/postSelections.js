const axios = require('axios').default;
const db = require('../db/models/index');
const { user, selection } = db;

const addUserToDB = async (userID, authorization) => {
  const { data } = await axios
    .create({
      headers: {
        authorization,
      },
    })
    .get('https://c-and-ct.auth0.com/userinfo');
  if (data.name) {
    const [first_name, last_name] = data.name.split(' ');
    await user.create({ id: userID, first_name, last_name });
  } else {
    await user.create({
      id: userID,
      first_name: data.nickname,
      last_name: data.email,
    });
  }
};

const selectionsHandler = async (req, res) => {
  const userID = req.user.sub;
  const { selections } = req.body;
  const userPicks = Object.keys(selections).map(key => ({
    user_id: userID,
    game_id: key,
    team_id: selections[key],
  }));

  let status = [];

  try {
    status = await Promise.all(userPicks.map(pick => selection.upsert(pick)));
  } catch (e) {
    if (e.message === 'cannot set user picks if game has been started') {
      console.log(
        `user (${userID}) attempted to save a pick after game had started ${JSON.stringify(
          userPicks,
        )}`,
      );
      return res.json({
        success: false,
        error: 'user attempted to save a pick after game had started',
      });
    }
    await addUserToDB(userID, req.headers.authorization);
    status = await Promise.all(userPicks.map(pick => selection.upsert(pick)));
  }

  return res.json({ success: true, created: status });
};

module.exports = selectionsHandler;
