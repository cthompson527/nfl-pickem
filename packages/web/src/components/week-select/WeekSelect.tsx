import React from 'react';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import useStyles from 'styles/styles';

interface Props {
  value: number;
  onChange: (newValue: number) => void;
}

export default ({ value, onChange }: Props) => {
  const classes = useStyles();
  const handleClick = (newValue: number) => {
    if (newValue < 1 || newValue > 17) {
      return;
    }
    onChange(newValue);
  };
  const largeGroup = (
    <ButtonGroup size="small" id="select-week" className={classes.buttonGroup}>
      <Button onClick={() => onChange(1)}>{'<<'}</Button>
      <Button disabled={value === 1} onClick={() => handleClick(value - 1)}>
        {'<'}
      </Button>
      <Button onClick={() => handleClick(value - 2)}>
        {value - 2 > 0 ? value - 2 : ' '}
      </Button>
      <Button onClick={() => handleClick(value - 1)}>
        {value - 1 > 0 ? value - 1 : ' '}
      </Button>
      <Button disabled>{value}</Button>
      <Button onClick={() => handleClick(value + 1)}>
        {value + 1 < 18 ? value + 1 : ' '}
      </Button>
      <Button onClick={() => handleClick(value + 2)}>
        {value + 2 < 18 ? value + 2 : ' '}
      </Button>
      <Button disabled={value === 17} onClick={() => onChange(value + 1)}>
        {'>'}
      </Button>
      <Button onClick={() => onChange(17)}>{'>>'}</Button>
    </ButtonGroup>
  );

  const mediumGroup = (
    <ButtonGroup size="small" id="select-week" className={classes.buttonGroup}>
      <Button onClick={() => onChange(1)}>{'<<'}</Button>
      <Button disabled={value === 1} onClick={() => handleClick(value - 1)}>
        {'<'}
      </Button>
      <Button onClick={() => handleClick(value - 1)}>
        {value - 1 > 0 ? value - 1 : ' '}
      </Button>
      <Button disabled>{value}</Button>
      <Button onClick={() => handleClick(value + 1)}>
        {value + 1 < 18 ? value + 1 : ' '}
      </Button>
      <Button disabled={value === 17} onClick={() => onChange(value + 1)}>
        {'>'}
      </Button>
      <Button onClick={() => onChange(17)}>{'>>'}</Button>
    </ButtonGroup>
  );

  const smallGroup = (
    <ButtonGroup size="small" id="select-week" className={classes.buttonGroup}>
      <Button disabled={value === 1} onClick={() => onChange(value - 1)}>
        {'<'}
      </Button>
      <Button disabled={value === 17} onClick={() => onChange(value + 1)}>
        {'>'}
      </Button>
    </ButtonGroup>
  );

  return (
    <>
      <Hidden smDown>{largeGroup}</Hidden>
      <Hidden only={['xs', 'md', 'lg', 'xl']}>{mediumGroup}</Hidden>
      <Hidden smUp>{smallGroup}</Hidden>
    </>
  );
};
