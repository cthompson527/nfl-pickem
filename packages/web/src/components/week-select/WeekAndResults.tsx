import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import WeekSelect from './WeekSelect';
import React from 'react';
import { Game } from 'components/games-table/Games';
import { Selected } from 'pages/week/Week';
import useStyles from 'styles/styles';
import Hidden from '@material-ui/core/Hidden';

interface Props {
  games: Game[];
  week: number;
  selections: Selected;
  setWeek: (newWeek: number) => void;
}

export const getWinner = ({
  AwayTeamScore,
  HomeTeamScore,
  AwayID,
  HomeID,
}: Game) => {
  if (AwayTeamScore == null || HomeTeamScore == null) {
    return null;
  }

  if (AwayTeamScore == HomeTeamScore) {
    return null;
  }

  return HomeTeamScore > AwayTeamScore ? HomeID : AwayID;
};

export default ({ games, week, selections, setWeek }: Props) => {
  const classes = useStyles();
  const gamesThisWeek = games.filter(g => g.Week === week);
  const correct = gamesThisWeek.reduce((total, current) => {
    if (
      getWinner(current) ===
      (selections[current.UUID] && selections[current.UUID].teamID)
    ) {
      return total + 1;
    }
    return total;
  }, 0);
  const incorrect = gamesThisWeek.reduce((total, current) => {
    if (
      getWinner(current) &&
      getWinner(current) !==
        (selections[current.UUID] && selections[current.UUID].teamID)
    ) {
      return total + 1;
    }
    return total;
  }, 0);

  return (
    <Grid container>
      <Grid item xs={3} className={classes.results}>
        <Typography variant="h6" className={classes.resultsText}>
          <Hidden smDown>Correct: </Hidden>
          <Hidden only={['xs', 'md', 'lg', 'xs']}>Right: </Hidden>
          <Hidden smUp>👍: </Hidden>
          {correct}
        </Typography>
      </Grid>
      <Grid item xs={6} className={classes.weekPicker}>
        <WeekSelect value={week} onChange={setWeek} />
      </Grid>
      <Grid item xs={3} className={classes.results}>
        <Typography variant="h6" className={classes.resultsText}>
          <Hidden smDown>Incorrect: </Hidden>
          <Hidden only={['xs', 'md', 'lg', 'xs']}>Wrong: </Hidden>
          <Hidden smUp>👎: </Hidden>
          {incorrect}
        </Typography>
      </Grid>
    </Grid>
  );
};
