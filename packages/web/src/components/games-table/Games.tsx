import React from 'react';
import Game from './Game';
import { Record, Records, Selected } from 'pages/week/Week';

export interface Game {
  AwayTeamLocation: string;
  AwayTeamName: string;
  AwayTeamShortName: string;
  HomeTeamLocation: string;
  HomeTeamName: string;
  HomeTeamShortName: string;
  HomeID: string;
  AwayID: string;
  AwayTeamScore?: number;
  HomeTeamScore?: number;
  TimePlayed?: string;
  Week: number;
  UUID: string;
  awayRecord: Record;
  homeRecord: Record;
}

interface Props {
  games: Game[];
  selections: Selected;
  teamRecords: Records;
}

function GamesTable({ games, selections, teamRecords }: Props) {
  return (
    <>
      {games.map(g => {
        return (
          <Game
            key={g.UUID}
            {...g}
            dbSelected={selections[g.UUID] && selections[g.UUID].teamID}
            awayRecord={teamRecords[g.AwayID]}
            homeRecord={teamRecords[g.HomeID]}
          />
        );
      })}
    </>
  );
}

export default GamesTable;
