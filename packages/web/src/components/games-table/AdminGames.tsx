import React from 'react';
import Grid from '@material-ui/core/Grid';
import useStyles from 'styles/styles';
import { Game } from 'components/games-table/Games';
import parseDate from 'date-fns/parseISO';
import getMonth from 'date-fns/getMonth';
import getDate from 'date-fns/getDate';
import getYear from 'date-fns/getYear';
import getHours from 'date-fns/getHours';
import getMinutes from 'date-fns/getMinutes';
import getDaysInMonth from 'date-fns/getDaysInMonth';
import { post } from 'api/Games';
import { useAuth0 } from 'react-auth0-wrapper';
import Button from '@material-ui/core/Button';
import WeekSelect from 'components/week-select/WeekSelect';
import Typography from '@material-ui/core/Typography';
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import TextField from '@material-ui/core/TextField';

interface Props {
  games: Game[];
  week: number;
  setWeek: (week: number) => void;
}

interface GameById {
  [id: string]: Game;
}

const twelveHourToTwentyFour = (hour: number, ampm: 'am' | 'pm') =>
  ampm === 'am' ? (hour === 12 ? 0 : hour) : hour === 12 ? hour : hour + 12;

const twentyFourHourToTwelve = (h24: number) => {
  const hour = h24 === 0 ? 12 : h24 > 12 ? h24 - 12 : h24;
  const ampm: 'am' | 'pm' = h24 < 12 ? 'am' : 'pm';
  return { hour, ampm };
};

const getTimeValues = (isoString?: string) => {
  const date = !!isoString ? parseDate(isoString) : new Date();
  const h24 = getHours(date);
  const { hour, ampm } = twentyFourHourToTwelve(h24);
  return {
    year: getYear(date),
    month: getMonth(date),
    date: getDate(date),
    hour,
    minute: getMinutes(date),
    ampm,
    second: 0,
  };
};

const mergeGame = (g: Game, modifiedGame?: Game) => {
  if (!modifiedGame) {
    return g;
  }
  return {
    AwayTeamLocation: modifiedGame.AwayTeamLocation || g.AwayTeamLocation,
    AwayTeamName: modifiedGame.AwayTeamName || g.AwayTeamName,
    AwayTeamShortName: modifiedGame.AwayTeamShortName || g.AwayTeamShortName,
    HomeTeamLocation: modifiedGame.HomeTeamLocation || g.HomeTeamLocation,
    HomeTeamName: modifiedGame.HomeTeamName || g.HomeTeamName,
    HomeTeamShortName: modifiedGame.HomeTeamShortName || g.HomeTeamShortName,
    AwayTeamScore: modifiedGame.AwayTeamScore || g.AwayTeamScore,
    HomeTeamScore: modifiedGame.HomeTeamScore || g.HomeTeamScore,
    TimePlayed: modifiedGame.TimePlayed || g.TimePlayed,
    UUID: g.UUID,
  };
};

function AdminGamesTable({ games, setWeek, week }: Props) {
  const [modifiedGames, setModifiedGames] = React.useState<GameById>({});
  const [edited, setEdited] = React.useState(false);
  const classes = useStyles();
  const { getTokenSilently } = useAuth0();

  const handleTimeChange = (gameID: string, date: MaterialUiPickersDate) => {
    if (!date) {
      return;
    }
    const timeObj = { TimePlayed: date.toISOString() };
    console.log(timeObj);
    const currentGame =
      modifiedGames[gameID] || games.find(g => g.UUID === gameID);

    const updatedGame = { ...currentGame, ...timeObj };
    setModifiedGames({ ...modifiedGames, [gameID]: updatedGame });
    setEdited(true);
  };

  const handleScoreChange = (
    gameID: string,
    homeAway: 'home' | 'away',
    score: number,
  ) => {
    if (Number.isNaN(score)) {
      return;
    }
    const scoreObj = {
      [homeAway === 'home' ? 'HomeTeamScore' : 'AwayTeamScore']: score,
    };
    const currentGame =
      modifiedGames[gameID] || games.find(g => g.UUID === gameID);

    const updatedGame = { ...currentGame, ...scoreObj };

    setModifiedGames({ ...modifiedGames, [gameID]: updatedGame });
    setEdited(true);
  };

  const handleSubmit = async () => {
    const token = await getTokenSilently();
    const data = await post(Object.values(modifiedGames), token);
    if (data.success) {
      window.location.reload();
    }
    console.log(data);
  };

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          {!edited && <WeekSelect value={week} onChange={setWeek} />}
          {edited && <Button onClick={handleSubmit}>Submit</Button>}
        </Grid>
      </Grid>
      {games.map(g => {
        const game = mergeGame(g, modifiedGames[g.UUID]);
        const { year, month, date, hour, minute, ampm } = getTimeValues(
          game.TimePlayed,
        );
        return (
          <Grid container key={game.UUID} className={classes.adminTableRow}>
            <Grid item xs={4} className={classes.rowCenter}>
              <DateTimePicker
                ampm
                value={game.TimePlayed}
                onChange={date => handleTimeChange(game.UUID, date)}
                label="Time Played"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="dense"
                format="MMM d yyyy h:mm a"
              />
            </Grid>
            <Grid item xs={4} className={classes.rowCenter}>
              <TextField
                id={`${game.UUID}away`}
                label={game.AwayTeamName}
                value={game.AwayTeamScore}
                onChange={e =>
                  handleScoreChange(game.UUID, 'away', Number(e.target.value))
                }
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="dense"
              />
            </Grid>
            <Grid item xs={4} className={classes.rowCenter}>
              <TextField
                id={`${game.UUID}home`}
                label={game.HomeTeamName}
                value={game.HomeTeamScore}
                onChange={e =>
                  handleScoreChange(game.UUID, 'home', Number(e.target.value))
                }
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="dense"
              />
            </Grid>
          </Grid>
        );
      })}
    </>
  );
}

export default (props: Props) => (
  <MuiPickersUtilsProvider utils={DateFnsUtils}>
    <AdminGamesTable {...props} />
  </MuiPickersUtilsProvider>
);
