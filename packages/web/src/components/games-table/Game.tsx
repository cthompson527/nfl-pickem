import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import useStyles from 'styles/styles';
import parseDate from 'date-fns/parseISO';
import formatDate from 'date-fns/format';
import { Game } from './Games';
import { post as postSelection } from 'api/Selections';
import { useAuth0 } from 'react-auth0-wrapper';
import isBefore from 'date-fns/isBefore';

interface IProps extends Game {
  dbSelected?: string;
}

enum SavingState {
  NONE,
  SAVING,
  SAVED,
  ERROR,
}

export default ({
  UUID,
  dbSelected,
  AwayTeamName,
  AwayTeamLocation,
  AwayTeamScore,
  TimePlayed,
  HomeTeamName,
  HomeTeamLocation,
  HomeTeamScore,
  HomeID,
  AwayID,
  awayRecord,
  homeRecord,
}: IProps) => {
  const [selected, setSelected] = React.useState(dbSelected);
  const [saved, setSaved] = React.useState(0);
  const classes = useStyles();
  const { getTokenSilently } = useAuth0();
  const gameHasStarted =
    TimePlayed && isBefore(new Date(TimePlayed), new Date());

  React.useEffect(() => {
    if (dbSelected) {
      const s = dbSelected === AwayID ? AwayTeamName : HomeTeamName;
      setSelected(s);
    }
  }, [dbSelected]);

  const handleSelected = async (newSelected: string) => {
    if (selected === newSelected) {
      return;
    }

    if (gameHasStarted) {
      return;
    }

    setSaved(SavingState.SAVING);
    const selectedID = HomeTeamName === newSelected ? HomeID : AwayID;
    const token = await getTokenSilently();
    const response = await postSelection(
      { selections: { [UUID]: selectedID } },
      token,
    );
    if (response.success === false) {
      console.error(response.error);
      setSaved(SavingState.ERROR);
      return;
    }
    setSaved(SavingState.SAVED);
    setSelected(newSelected);
    setTimeout(() => setSaved(SavingState.NONE), 2000);
  };

  const getCSSClasses = (homeAway: 'home' | 'away') => {
    const css: string[] = [];
    const hasWinner =
      AwayTeamScore != null &&
      HomeTeamScore != null &&
      AwayTeamScore !== HomeTeamScore;

    if (gameHasStarted) {
      css.push(classes.teamCompleted);
    } else {
      css.push(classes.team);
    }

    if (homeAway === 'home') {
      css.push(classes.homeTeam);
    }

    if (hasWinner) {
      // @ts-ignore
      const awayWon = AwayTeamScore > HomeTeamScore;
      if (awayWon && homeAway === 'away' && selected === AwayTeamName) {
        css.push(classes.teamCompletedCorrect);
      } else if (!awayWon && homeAway === 'home' && selected === HomeTeamName) {
        css.push(classes.teamCompletedCorrect);
      } else if (homeAway === 'away' && selected === AwayTeamName) {
        css.push(classes.teamCompletedWrong);
      } else if (homeAway === 'home' && selected === HomeTeamName) {
        css.push(classes.teamCompletedWrong);
      }
    } else {
      if (selected === AwayTeamName) {
        if (homeAway === 'away') {
          if (gameHasStarted) {
            css.push(classes.teamCompletedSelected);
          } else {
            css.push(classes.teamSelected);
          }
        }
      }
      if (selected === HomeTeamName) {
        if (homeAway === 'home') {
          if (gameHasStarted) {
            css.push(classes.teamCompletedSelected);
          } else {
            css.push(classes.teamSelected);
          }
        }
      }
    }
    return css;
  };

  const awayRecordString = awayRecord
    ? `Record: ${awayRecord.wins} - ${awayRecord.losses} ${
        awayRecord.ties > 0 ? `- ${awayRecord.ties}` : ''
      }`
    : '';

  const homeRecordString = homeRecord
    ? `Record: ${homeRecord.wins} - ${homeRecord.losses} ${
        homeRecord.ties > 0 ? `- ${homeRecord.ties}` : ''
      }`
    : '';

  const renderScores = () => (
    <Grid container>
      <Grid item xs={6} sm={5}>
        <Grid container className={getCSSClasses('away').join(' ')}>
          <Hidden smDown>
            <Grid item md={5}>
              <div>{AwayTeamLocation}</div>
              {awayRecord && (
                <div className={classes.record}>{awayRecordString}</div>
              )}
            </Grid>
          </Hidden>
          <Grid item md={5} xs={10}>
            <div>{AwayTeamName}</div>
            <Hidden mdUp>
              {awayRecord ? (
                <div className={classes.record}>{awayRecordString}</div>
              ) : null}
            </Hidden>
          </Grid>
          <Grid item xs={2}>
            {AwayTeamScore}
          </Grid>
        </Grid>
      </Grid>
      <Hidden only="xs">
        <Grid item sm={2} className={classes.centerOfTeams}>
          <div className={classes.timePlayed}>
            {TimePlayed && formatDate(parseDate(TimePlayed), 'EEEE hh:mm a')}
          </div>
        </Grid>
      </Hidden>
      <Grid item xs={6} sm={5}>
        <Grid container className={getCSSClasses('home').join(' ')}>
          <Grid item xs={2}>
            {HomeTeamScore}
          </Grid>
          <Grid item md={5} xs={10}>
            {HomeTeamName}
          </Grid>
          <Hidden smDown>
            <Grid item md={5}>
              {HomeTeamLocation}
            </Grid>
          </Hidden>
          {homeRecord && (
            <Grid item xs={12} className={classes.record}>
              {homeRecordString}
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );

  const renderNoScores = () => (
    <Grid container>
      <Grid item xs={5}>
        <Grid
          container
          onClick={() => handleSelected(AwayTeamName)}
          className={getCSSClasses('away').join(' ')}
        >
          <Hidden smDown>
            <Grid item md={5}>
              {AwayTeamLocation}
            </Grid>
          </Hidden>
          <Grid item md={5} xs={12}>
            {AwayTeamName}
          </Grid>
          <Hidden xsDown>
            <Grid item md={2}>
              {AwayTeamScore}
            </Grid>
          </Hidden>
          {awayRecord && (
            <Grid item xs={12} className={classes.record}>
              {awayRecordString}
            </Grid>
          )}
        </Grid>
      </Grid>
      <Grid item xs={2} className={classes.centerOfTeams}>
        <div
          className={`${classes.saveState} ${
            saved === SavingState.NONE
              ? classes.saveStateShow
              : classes.saveStateHide
          }`}
        >
          <Hidden only="xs">
            <div className={classes.timePlayed}>
              {TimePlayed && formatDate(parseDate(TimePlayed), 'EEEE hh:mm a')}
            </div>
          </Hidden>
          <Hidden smUp>
            <div className={classes.timePlayed}>
              {TimePlayed && formatDate(parseDate(TimePlayed), 'hh:mm a')}
            </div>
          </Hidden>
        </div>
        <div
          className={`${classes.saveState} ${
            saved === SavingState.NONE
              ? classes.saveStateHide
              : classes.saveStateShow
          }`}
        >
          <div className={classes.saveMessage}>
            {saved === SavingState.SAVING
              ? 'saving'
              : saved === SavingState.ERROR
              ? 'error'
              : 'saved'}
          </div>
        </div>
      </Grid>
      <Grid item xs={5}>
        <Grid
          container
          onClick={() => handleSelected(HomeTeamName)}
          className={getCSSClasses('home').join(' ')}
        >
          <Hidden xsDown>
            <Grid item md={2}>
              {HomeTeamScore}
            </Grid>
          </Hidden>
          <Grid item md={5} xs={12}>
            {HomeTeamName}
          </Grid>
          <Hidden smDown>
            <Grid item md={5}>
              {HomeTeamLocation}
            </Grid>
          </Hidden>
          {homeRecord && (
            <Grid item xs={12} className={classes.record}>
              {homeRecordString}
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <>
      {!!AwayTeamScore || !!HomeTeamScore ? renderScores() : renderNoScores()}
    </>
  );
};
