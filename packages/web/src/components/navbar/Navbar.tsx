import React from 'react';
import { useAuth0, checkPermissions } from 'react-auth0-wrapper';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';
import Tab from '@material-ui/core/Tab';
import { Link as RouterLink } from 'react-router-dom';
import useStyles from 'styles/styles';
import Hidden from '@material-ui/core/Hidden';

const N = () => {
  const {
    isAuthenticated,
    loginWithRedirect,
    logout,
    loading,
    token,
  } = useAuth0();
  const classes = useStyles();
  const canCreateGames = checkPermissions(token, 'create:games');
  return (
    <AppBar position="static">
      <Toolbar>
        <Link component={RouterLink} to="/" className={classes.navbarTitle}>
          <Typography variant="h6">
            NFL<Hidden xsDown> Pickem</Hidden>
          </Typography>
        </Link>
        <div className={classes.navbarSpacing} />
        {loading ? (
          <div />
        ) : isAuthenticated ? (
          <>
            <Link component={RouterLink} to="/league">
              <Tab className={classes.navbarTab} label="League" />
            </Link>
            <Link component={RouterLink} to="/week">
              <Tab className={classes.navbarTab} label="Week" />
            </Link>
            {canCreateGames && (
              <Link component={RouterLink} to="/admin/week">
                <Tab className={classes.navbarTab} label="Admin" />
              </Link>
            )}
            <Tab
              className={classes.rightNavbarTab}
              label="Logout"
              onClick={() => logout()}
            />
          </>
        ) : (
          <Tab
            className={classes.navbarTab}
            label="Login"
            onClick={() => loginWithRedirect()}
          />
        )}
      </Toolbar>
    </AppBar>
  );
};

export default N;
