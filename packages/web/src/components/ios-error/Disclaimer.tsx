import React from 'react';
import Grid from '@material-ui/core/Grid';
import useStyles from 'styles/styles';
import Typography from '@material-ui/core/Typography';

const ios = 'iPhone';
const safari = 'Safari';

export default () => {
  const classes = useStyles();
  const { userAgent } = window.navigator;
  const usingiOSSafari = userAgent.includes(ios) && userAgent.includes(safari);
  const [showDisclaimer, setShowDisclaimer] = React.useState(usingiOSSafari);
  return (
    <Grid
      container
      className={`${classes.page} ${classes.warningTransition} ${
        showDisclaimer ? classes.warning : classes.warningHide
      }`}
      onClick={() => setShowDisclaimer(false)}
    >
      <Grid item xs={12}>
        <Typography variant="subtitle1">
          There is a problem with Mobile Safari staying logged in. Please use a
          different browser for a better user experience.
        </Typography>
      </Grid>
    </Grid>
  );
};
