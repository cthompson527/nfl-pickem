import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import useStyles from 'styles/styles';
import Button from '@material-ui/core/Button';
import { useAuth0 } from 'react-auth0-wrapper';

export default () => {
  const classes = useStyles();
  const { loginWithRedirect } = useAuth0();
  return (
    <div style={{margin: '2em'}}>
      <Paper className={classes.loginCard}>
        <Typography variant="h6">
          You must{' '}
          <Button
            variant="contained"
            color="primary"
            className={classes.loginButton}
            onClick={() => loginWithRedirect()}
          >
            Login
          </Button>{' '}
          to continue.
        </Typography>
        <Typography variant="body2" style={{ marginTop: '3em' }}>
          By signing up, you are agreeing with our{' '}
          <a href="https://app.termly.io/document/privacy-policy/1adf04de-d2ae-45e7-b2bf-978a2478b44b">
            Privacy Policy
          </a>{' '}
          and{' '}
          <a href="https://app.termly.io/document/cookie-policy/13f0867a-e22e-44b8-b8e7-fee0b420a83f">
            Cookie Policy
          </a>
          .
        </Typography>
      </Paper>
    </div>
  );
};
