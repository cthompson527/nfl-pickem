import React from 'react';
import { get } from 'api/Games';
import AdminGamesTable from 'components/games-table/AdminGames';
import { Game } from 'components/games-table/Games';
import { useAuth0 } from 'react-auth0-wrapper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import useStyles from 'styles/styles';
import { getCurrentWeek } from 'pages/week/Week';

export default () => {
  const [week, setWeek] = React.useState(getCurrentWeek());
  const [games, setGames] = React.useState<Game[]>([]);
  const [token, setToken] = React.useState();
  const { getTokenSilently } = useAuth0();
  const classes = useStyles();

  React.useEffect(() => {
    const fetchGames = async () => {
      const token = await getTokenSilently();
      setToken(token);

      const g = await get(token, week);
      setGames(g);
    };
    fetchGames();
  }, [week, token]);

  return (
    <div className={classes.page}>
      <div>
        <Typography variant="h4" className={classes.pageTitle}>
          Edit Games Week {week}
        </Typography>
        <Paper className={classes.weekCard}>
          <AdminGamesTable games={games} setWeek={setWeek} week={week} />
        </Paper>
      </div>
    </div>
  );
};
