import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { getWinner } from 'components/week-select/WeekAndResults';
import { Game } from 'components/games-table/Games';
import useStyles from 'styles/styles';
import { useAuth0 } from 'react-auth0-wrapper';
import { getOpponentSelections } from 'api/Selections';
import { get as getUsers } from 'api/Users';
import { get as getGames } from 'api/Games';
import { Selection, hasWinner } from 'pages/opponent/Opponent';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';

interface Row {
  id: string;
  name: string;
  points: number;
}

interface Games {
  [id: string]: Game;
}

interface DBUser {
  id: string;
  first_name: string;
  last_name: string;
}

export default () => {
  const [rows, setRows] = React.useState<Row[]>([]);
  const { getTokenSilently, user } = useAuth0();
  const classes = useStyles({});
  console.log(user);

  React.useEffect(() => {
    (async () => {
      const token = await getTokenSilently();
      const selections = await getOpponentSelections(token);
      const users = (await getUsers(token)) as DBUser[];
      const games = await getGames(token);

      const userPoints: { [userID: string]: number } = {};
      users.forEach((u: DBUser) => (userPoints[u.id] = 0));

      const gameObj: Games = {};
      games.forEach((g: Game) => (gameObj[g.UUID] = g));

      selections.forEach((s: Selection) => {
        const game = gameObj[s.gameID];
        if (!hasWinner(game)) {
          return;
        }
        if (getWinner(game) === s.teamID) {
          userPoints[s.userID] += 1;
        }
      });

      setRows(
        users.map((u: DBUser) => ({
          id: u.id,
          name: `${u.first_name} ${u.last_name}`,
          points: userPoints[u.id],
        })),
      );

      console.log(selections, users, gameObj, userPoints);
    })();
  }, []);

  const getLink = (rowID: string) => {
    if (rowID === user.sub) {
      return '/week';
    }

    return `/opponent/${rowID}`;
  };

  // @ts-ignore
  return (
    <div className={classes.page}>
      <Typography variant="h4" className={classes.pageTitle}>
        League
      </Typography>
      <Paper className={classes.weekCard}>
        <Grid container justify="space-between" className={classes.leagueRow}>
          <Typography variant="subtitle1">Name</Typography>
          <Typography variant="subtitle1">Points</Typography>
          {rows.map(row => (
            <Link to={getLink(row.id)} className={classes.leagueRow}>
              <Grid container justify="space-between" style={{flexWrap: 'nowrap'}}>
                <Typography variant="body2" className={classes.leagueName}>
                  {row.name}
                </Typography>
                <Typography variant="body2">{row.points}</Typography>
              </Grid>
            </Link>
          ))}
        </Grid>
      </Paper>
    </div>
  );
};
