import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Game as IGame } from 'components/games-table/Games';
import useStyles from 'styles/styles';
import { useAuth0 } from 'react-auth0-wrapper';
import { getCurrentWeek, Selected } from 'pages/week/Week';
import Grid from '@material-ui/core/Grid';
import WeekSelect from '../../components/week-select/WeekSelect';
import { get as getGames } from 'api/Games';
import { getOpponentSelections } from 'api/Selections';
import { RouteComponentProps } from 'react-router-dom';
import Hidden from '@material-ui/core/Hidden';
import formatDate from 'date-fns/format';
import parseDate from 'date-fns/parseISO';
import WeekAndResults from 'components/week-select/WeekAndResults';

export interface Selections {
  [gameID: string]: Selection;
}

export interface Selection {
  gameID: string;
  teamID: string;
  userID: string;
}

export const hasWinner = ({ AwayTeamScore, HomeTeamScore }: IGame) =>
  AwayTeamScore != null &&
  HomeTeamScore != null &&
  AwayTeamScore !== HomeTeamScore;

export default ({ match }: RouteComponentProps<{ id: string }>) => {
  console.log(match);
  const [week, setWeek] = React.useState(getCurrentWeek());
  const [games, setGames] = React.useState<IGame[]>([]);
  const [selections, setSelections] = React.useState<Selected>({});
  const classes = useStyles();
  const { getTokenSilently } = useAuth0();

  React.useEffect(() => {
    const fetchGames = async () => {
      const token = await getTokenSilently();

      const g = await getGames(token);
      setGames(g);
    };
    fetchGames();
  }, []);

  React.useEffect(() => {
    const fetchSelections = async () => {
      const token = await getTokenSilently();

      const opponentSelections = await getOpponentSelections(
        token,
        match.params.id,
      );
      const selectionsObj: Selections = {};
      opponentSelections.forEach(
        (s: { gameID: string; teamID: string; userID: string }) => {
          selectionsObj[s.gameID] = {
            gameID: s.gameID,
            teamID: s.teamID,
            userID: s.userID,
          };
        },
      );
      setSelections(selectionsObj);
    };
    fetchSelections();
  }, []);

  const WeekSelectAndButtons = () => {
    return (
      <Grid container>
        <Grid item xs={6}>
          <WeekSelect value={week} onChange={setWeek} />
        </Grid>
      </Grid>
    );
  };

  return (
    <div className={classes.page}>
      <div>
        <Typography variant="h4" className={classes.pageTitle}>
          Week {week}
        </Typography>
        <Paper className={classes.weekCard}>
          <WeekAndResults
            games={games.filter(g => g.Week === week)}
            week={week}
            selections={selections}
            setWeek={setWeek}
          />
          <GamesTable
            games={games.filter(g => g.Week === week)}
            selections={selections}
          />
        </Paper>
      </div>
    </div>
  );
};

interface Props {
  games: IGame[];
  selections: Selected;
}

function GamesTable({ games, selections }: Props) {
  return (
    <>
      {games.map(g => (
        <Game
          key={g.UUID}
          {...g}
          dbSelected={selections[g.UUID] && selections[g.UUID].teamID}
        />
      ))}
    </>
  );
}

interface IProps extends IGame {
  dbSelected?: string;
}

function Game(game: IProps) {
  const {
    UUID,
    dbSelected,
    AwayTeamName,
    AwayTeamLocation,
    AwayTeamScore,
    TimePlayed,
    HomeTeamName,
    HomeTeamLocation,
    HomeTeamScore,
    HomeID,
    AwayID,
  } = game;
  const classes = useStyles();
  let selected: string = '';
  if (dbSelected) {
    selected = dbSelected === AwayID ? AwayTeamName : HomeTeamName;
  }

  const getCSSClasses = (homeAway: 'home' | 'away') => {
    const css: string[] = [];

    css.push(classes.teamCompleted);

    if (homeAway === 'home') {
      css.push(classes.homeTeam);
    }

    if (hasWinner(game)) {
      // @ts-ignore
      const awayWon = AwayTeamScore > HomeTeamScore;
      if (awayWon && homeAway === 'away' && selected === AwayTeamName) {
        css.push(classes.teamCompletedCorrect);
      } else if (!awayWon && homeAway === 'home' && selected === HomeTeamName) {
        css.push(classes.teamCompletedCorrect);
      } else if (homeAway === 'away' && selected === AwayTeamName) {
        css.push(classes.teamCompletedWrong);
      } else if (homeAway === 'home' && selected === HomeTeamName) {
        css.push(classes.teamCompletedWrong);
      }
    } else {
      if (selected === AwayTeamName) {
        if (homeAway === 'away') {
          css.push(classes.teamCompletedSelected);
        }
      }
      if (selected === HomeTeamName) {
        if (homeAway === 'home') {
          css.push(classes.teamCompletedSelected);
        }
      }
    }
    return css;
  };

  const renderScores = () => (
    <Grid container>
      <Grid item xs={6} sm={5}>
        <Grid container className={getCSSClasses('away').join(' ')}>
          <Hidden smDown>
            <Grid item md={5}>
              {AwayTeamLocation}
            </Grid>
          </Hidden>
          <Grid item md={5} xs={10}>
            {AwayTeamName}
          </Grid>
          <Grid item xs={2}>
            {AwayTeamScore}
          </Grid>
        </Grid>
      </Grid>
      <Hidden only="xs">
        <Grid item sm={2} className={classes.centerOfTeams}>
          <div className={classes.timePlayed}>
            {TimePlayed && formatDate(parseDate(TimePlayed), 'EEEE hh:mm a')}
          </div>
        </Grid>
      </Hidden>
      <Grid item xs={6} sm={5}>
        <Grid container className={getCSSClasses('home').join(' ')}>
          <Grid item xs={2}>
            {HomeTeamScore}
          </Grid>
          <Grid item md={5} xs={10}>
            {HomeTeamName}
          </Grid>
          <Hidden smDown>
            <Grid item md={5}>
              {HomeTeamLocation}
            </Grid>
          </Hidden>
        </Grid>
      </Grid>
    </Grid>
  );

  const renderNoScores = () => (
    <Grid container>
      <Grid item xs={5}>
        <Grid container className={getCSSClasses('away').join(' ')}>
          <Hidden smDown>
            <Grid item md={5}>
              {AwayTeamLocation}
            </Grid>
          </Hidden>
          <Grid item md={5} xs={12}>
            {AwayTeamName}
          </Grid>
        </Grid>
        <Hidden xsDown>
          <Grid item md={2}>
            {AwayTeamScore}
          </Grid>
        </Hidden>
      </Grid>
      <Grid item xs={2} className={classes.centerOfTeams}>
        <Hidden only="xs">
          <div className={classes.timePlayed}>
            {TimePlayed && formatDate(parseDate(TimePlayed), 'EEEE hh:mm a')}
          </div>
        </Hidden>
        <Hidden smUp>
          <div className={classes.timePlayed}>
            {TimePlayed && formatDate(parseDate(TimePlayed), 'hh:mm a')}
          </div>
        </Hidden>
      </Grid>
      <Grid item xs={5}>
        <Grid container className={getCSSClasses('home').join(' ')}>
          <Hidden xsDown>
            <Grid item md={2}>
              {HomeTeamScore}
            </Grid>
          </Hidden>
          <Grid item md={5} xs={12}>
            {HomeTeamName}
          </Grid>
          <Hidden smDown>
            <Grid item md={5}>
              {HomeTeamLocation}
            </Grid>
          </Hidden>
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <>{!!AwayTeamScore || !!HomeTeamScore ? renderScores() : renderNoScores()}</>
  );
}
