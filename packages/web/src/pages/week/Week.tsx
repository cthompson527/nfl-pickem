import React from 'react';
import WeekSelect from 'components/week-select/WeekSelect';
import { get as getGames } from 'api/Games';
import { get as getSelections } from 'api/Selections';
import GamesTable, { Game } from 'components/games-table/Games';
import { useAuth0 } from 'react-auth0-wrapper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import useStyles from 'styles/styles';
import isAfter from 'date-fns/isAfter';
import WeekAndResults from 'components/week-select/WeekAndResults';
import { hasWinner } from '../opponent/Opponent';

export interface Selected {
  [gameID: string]: { gameID: string; teamID: string; teamName?: string };
}

export interface Records {
  [teamID: string]: Record;
}

export interface Record {
  wins: number;
  losses: number;
  ties: number;
}

export const getCurrentWeek = () => {
  const weekStarts = [
    new Date(2019, 8, 5),
    new Date(2019, 8, 12),
    new Date(2019, 8, 19),
    new Date(2019, 8, 26),
    new Date(2019, 9, 3),
    new Date(2019, 9, 10),
    new Date(2019, 9, 17),
    new Date(2019, 9, 24),
    new Date(2019, 9, 31),
    new Date(2019, 10, 7),
    new Date(2019, 10, 14),
    new Date(2019, 10, 21),
    new Date(2019, 10, 28),
    new Date(2019, 11, 5),
    new Date(2019, 11, 12),
    new Date(2019, 11, 19),
    new Date(2019, 11, 26),
  ];

  const date = new Date();
  let count = 1;
  while (isAfter(date, weekStarts[count])) {
    count += 1;
    if (count === 17) {
      return 17;
    }
  }

  return count;
};

export default () => {
  const [week, setWeek] = React.useState(getCurrentWeek());
  const [games, setGames] = React.useState<Game[]>([]);
  const [selections, setSelections] = React.useState<Selected>({});
  const [teamRecords, setTeamRecords] = React.useState<Records>({});
  const { getTokenSilently } = useAuth0();
  const classes = useStyles();

  React.useEffect(() => {
    const fetchGames = async () => {
      const token = await getTokenSilently();

      const g = await getGames(token);

      const records: Records = {};
      for (const game of g) {
        if (!records[game.AwayID]) {
          records[game.AwayID] = { wins: 0, losses: 0, ties: 0 };
        }

        if (!records[game.HomeID]) {
          records[game.HomeID] = { wins: 0, losses: 0, ties: 0 };
        }

        if (game.AwayTeamScore != null && game.HomeTeamScore != null) {
          if (game.AwayTeamScore > game.HomeTeamScore) {
            records[game.AwayID].wins = records[game.AwayID].wins + 1;
            records[game.HomeID].losses = records[game.HomeID].losses + 1;
          } else if (game.HomeTeamScore > game.AwayTeamScore) {
            records[game.AwayID].losses = records[game.AwayID].losses + 1;
            records[game.HomeID].wins = records[game.HomeID].wins + 1;
          } else {
            records[game.AwayID].ties = records[game.AwayID].ties + 1;
            records[game.HomeID].ties = records[game.HomeID].ties + 1;
          }
        }
      }

      setGames(g);
      setTeamRecords(records);
    };
    fetchGames();
  }, []);

  React.useEffect(() => {
    const fetchSelections = async () => {
      const token = await getTokenSilently();

      const s = await getSelections(token);
      setSelections({ ...s });
    };
    fetchSelections();
  }, [week]);

  return (
    <div className={classes.page}>
      <div>
        <Typography variant="h4" className={classes.pageTitle}>
          Week {week}
        </Typography>
        <Paper className={classes.weekCard}>
          <WeekAndResults
            games={games.filter(g => g.Week === week)}
            week={week}
            selections={selections}
            setWeek={setWeek}
          />
          <GamesTable
            games={games.filter(g => g.Week === week)}
            selections={selections}
            teamRecords={teamRecords}
          />
        </Paper>
      </div>
    </div>
  );
};
