import React from 'react';
import './App.css';
import Navbar from 'components/navbar/Navbar';
import Routes from 'components/routes/Routes';
import IOSDisclaimer from 'components/ios-error/Disclaimer';

const App: React.FC = () => {
  return (
    <>
      <Navbar />
      <IOSDisclaimer />
      <Routes />
    </>
  );
};

export default App;
