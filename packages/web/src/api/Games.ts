import axios from 'axios';
import { Game } from 'components/games-table/Games';

export const get = async (token: string, week?: number) => {
  const { data } = await axios.get(`/api/games${week ? `?week=${week}` : ''}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const games: Game[] = data.map((g: any) => ({
    UUID: g.id,
    AwayTeamLocation: g.away.location,
    AwayTeamName: g.away.name,
    AwayTeamShortName: g.away.short_name,
    HomeTeamLocation: g.home.location,
    HomeTeamName: g.home.name,
    HomeTeamShortName: g.home.short_name,
    HomeTeamScore: g.home_score,
    AwayTeamScore: g.away_score,
    HomeID: g.home_team,
    AwayID: g.away_team,
    TimePlayed: g.time_played,
    Week: g.week.week,
  }));

  return games;
};

export const post = async (games: Game[], token: string) => {
  const dbGames = games.map(g => ({
    id: g.UUID,
    time_played: g.TimePlayed,
    home_score: g.HomeTeamScore,
    away_score: g.AwayTeamScore,
  }));
  const { data } = await axios.post('/api/games', dbGames, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
};
