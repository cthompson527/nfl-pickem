import axios from 'axios';

export const get = async (token: string) => {
  const { data } = await axios.get('/api/users', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
};
