import axios from 'axios';
import { Selections } from 'pages/opponent/Opponent';

export const get = async (token: string) => {
  const { data } = await axios.get('/api/selections', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const selections: Selections = {};

  data.forEach((s: any) => {
    selections[s.game_id] = {
      gameID: s.game_id,
      teamID: s.team_id,
      userID: s.user_id,
    };
  });
  return selections;
};

export const post = async (
  selection: { selections: { [gameID: string]: string } },
  token: string,
) => {
  const { data } = await axios.post('/api/selections', selection, {
    headers: { Authorization: `Bearer ${token}` },
  });

  return data;
};

export const getOpponentSelections = async (
  token: string,
  opponentID?: string,
) => {
  const { data } = await axios.get(
    `/api/opponentSelections${opponentID ? `?id=${opponentID}` : ''}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
  return data.map(
    (d: { game_id: string; team_id: string; user_id: string }) => ({
      gameID: d.game_id,
      teamID: d.team_id,
      userID: d.user_id,
    }),
  );
};
